
---------test_data_1000_only_batt

[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 23:01:09.521'}]
---------help
{
    "satelliteId": "1000",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 23:01:09.521"
}
Alert -  BATTERY voltage is LOW
[]

---------test_data_1000_only_tstat

[]
[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 23:01:38.001'}]
---------help
{
    "satelliteId": "1000",
    "severity": "RED HIGH",
    "component": "TSTAT",
    "timestamp": "20180101 23:01:38.001"
}
Alert -  Satellite Temperature is too HIGH

---------test_data_1001_only_batt

[{'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 23:01:09.521'}]
---------help
{
    "satelliteId": "1001",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 23:01:09.521"
}
Alert -  BATTERY voltage is LOW
[]

---------test_data_1001_only_both

[{'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 23:01:09.521'}]
---------help
{
    "satelliteId": "1001",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 23:01:09.521"
}
Alert -  BATTERY voltage is LOW
[{'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 23:01:38.001'}]
---------help
{
    "satelliteId": "1001",
    "severity": "RED HIGH",
    "component": "TSTAT",
    "timestamp": "20180101 23:01:38.001"
}
Alert -  Satellite Temperature is too HIGH
