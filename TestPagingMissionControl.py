#!/usr/bin/env python3

import unittest
import main
import input

# Run Test Commands

"""
python3 -m unittest TestPagingMissionControl
"""

class TestPagingMissionContol(unittest.TestCase):

    #works
    def test_data_no_file(self):
        
        print("\n---------test_data_no_file\n")
        
        main.main("a")
                
    #works            
    def test_data_mix_of_1000_1001_data_but_only_1000_alerted(self):
        
        print("\n---------test_data_mix_of_1000_1001_data_but_only_1000_alerted\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_mix_of_1000_1001_data_but_only_1000_alerted.txt")

        text_file = open("expected_data/expected_mix_of_1000_1001_data_but_only_1000_alerted.txt", "r")
        data = text_file.read()
        text_file.close()
        
        self.assertTrue(actualBATT in data)
        self.assertTrue(actualTSTAT in data)
         
    # works    
    def test_data_none(self):
        
        print("\n--------test_data_none\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_none.txt")

        text_file = open("expected_data/expected_none.txt", "r")
        data = text_file.read()
        text_file.close()
         
    # works
    def test_data_empty(self):
        
        print("\n--------test_data_empty\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_empty.txt")
               
    #works     
    def test_data_both_1000_1001(self):
        
        print("\n---------test_data_both_1000_1001\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_both_1000_1001.txt")

        text_file = open("expected_data/expected_both_1000_1001.txt", "r")
        data = text_file.read()
        text_file.close()
        
        self.assertTrue(actualBATT in data)
        self.assertTrue(actualTSTAT in data)
            
    #works    
    def test_data_1001_only_both(self):
        
        print("\n---------test_data_1001_only_both\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_1001_only_both.txt")

        text_file = open("expected_data/expected_1001_only_both.txt", "r")
        data = text_file.read()
        text_file.close()
        
        self.assertTrue(actualBATT in data)
        self.assertTrue(actualTSTAT in data)
            
    #works    
    def test_data_1001_only_batt(self):
        
        print("\n---------test_data_1001_only_batt\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_1001_only_batt.txt")

        text_file = open("expected_data/expected_1001_batt.txt", "r")
        data = text_file.read()
        text_file.close()
        
        self.assertTrue(actualBATT in data)
           
    #works    
    def test_data_1000_only_tstat(self):
        
        print("\n---------test_data_1000_only_tstat\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_1000_only_tstat.txt")

        text_file = open("expected_data/expected_1000_tstat.txt", "r")
        data = text_file.read()
        text_file.close()
        
        self.assertTrue(actualTSTAT in data)
        
    #works
    def test_data_1000_only_batt(self):
        
        print("\n---------test_data_1000_only_batt\n")
        
        actualBATT, actualTSTAT = main.main("test_data/test_data_1000_only_batt.txt")

        text_file = open("expected_data/expected_1000_batt.txt", "r")
        data = text_file.read()
        text_file.close()
        
        self.assertTrue(actualBATT in data)
        