
---------test_data_1000_only_batt

[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 23:01:09.521'}]
{
    "satelliteId": "1000",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 23:01:09.521"
}
[]
Alert -  BATTERY voltage is LOW

---------test_data_1000_only_tstat

[]
[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 23:01:38.001'}]
{
    "satelliteId": "1000",
    "severity": "RED HIGH",
    "component": "TSTAT",
    "timestamp": "20180101 23:01:38.001"
}
Alert -  Satellite Temperature is too HIGH

---------test_data_1001_only_batt

[{'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 23:01:09.521'}]
{
    "satelliteId": "1001",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 23:01:09.521"
}
[]
Alert -  BATTERY voltage is LOW

---------test_data_1001_only_both

[{'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 23:01:09.521'}]
{
    "satelliteId": "1001",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 23:01:09.521"
}
[{'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 23:01:38.001'}]
{
    "satelliteId": "1001",
    "severity": "RED HIGH",
    "component": "TSTAT",
    "timestamp": "20180101 23:01:38.001"
}
Alert -  BATTERY voltage is LOW
Alert -  Satellite Temperature is too HIGH

---------test_data_both_1000_1001

[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 13:01:09.521'}, {'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 13:05:07.421'}]
{
    "satelliteId": "1000",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 13:01:09.521"
}
{
    "satelliteId": "1001",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 13:05:07.421"
}
[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 13:01:38.001'}, {'satellite_id': '1001', 'count': 3, 'timestamp': '20180101 23:01:38.001'}]
{
    "satelliteId": "1000",
    "severity": "RED HIGH",
    "component": "TSTAT",
    "timestamp": "20180101 13:01:38.001"
}
{
    "satelliteId": "1001",
    "severity": "RED HIGH",
    "component": "TSTAT",
    "timestamp": "20180101 23:01:38.001"
}
Alert -  BATTERY voltage is LOW
Alert -  Satellite Temperature is too HIGH

--------test_data_empty

[]
[]

---------test_data_mix_of_1000_1001_data_but_only_1000_alerted

[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 23:01:09.521'}]
{
    "satelliteId": "1000",
    "severity": "RED LOW",
    "component": "BATT",
    "timestamp": "20180101 23:01:09.521"
}
[{'satellite_id': '1000', 'count': 3, 'timestamp': '20180101 23:01:38.001'}]
{
    "satelliteId": "1000",
    "severity": "RED HIGH",
    "component": "TSTAT",
    "timestamp": "20180101 23:01:38.001"
}
Alert -  BATTERY voltage is LOW
Alert -  Satellite Temperature is too HIGH

---------test_data_no_file

File path a does not exist. Exiting...

--------test_data_none

[]
[]
