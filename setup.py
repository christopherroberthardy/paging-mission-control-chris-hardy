#!/usr/bin/env python
import setuptools

with open("README.md", "r") as fh:

    long_description = fh.read()

setuptools.setup(

    name="christopherroberthardy", # Replace with your username

    version="1.0.0",

    author="Christopher Hardy",

    author_email="christopherhardy@gmail.com>",

    description="<Template Setup.py package>",

    long_description=long_description,

    long_description_content_type="text/markdown",

    url="https://gitlab.com/christopherroberthardy/paging-mission-control-chris-hardy",

    packages=setuptools.find_packages(),

    classifiers=[

        "Programming Language :: Python :: 3",

        "License :: OSI Approved :: MIT License",

        "Operating System :: OS Independent",

    ],

    python_requires='>=3.6',

)