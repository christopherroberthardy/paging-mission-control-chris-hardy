#!/usr/bin/env python

import json

def printSateliteAlert(listOfSatellites,component):
    
    print(listOfSatellites)
    result=False
    json_object={}
    
    for p in listOfSatellites: 
                
        countOfSateliteAlertsDicts = {}
        
        if "satellite_id" in p:
            countOfSateliteAlertsDicts["satelliteId"]=p["satellite_id"]
            
            if component == "TSTAT":
                countOfSateliteAlertsDicts["severity"]="RED HIGH"
            elif component == "BATT":        
                countOfSateliteAlertsDicts["severity"]="RED LOW"
                
            countOfSateliteAlertsDicts["component"]=component
            countOfSateliteAlertsDicts["timestamp"]=p["timestamp"]
            
        #    print(countOfSateliteAlertsDicts)

            json_object = json.dumps(countOfSateliteAlertsDicts, indent = 4) 
            
            print(json_object)
            
            result=True
        else:
            print("No Satellite ID - No Satellite Alerted")
            
            result=False
    return result, json_object

# example final output

"""
[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
   }
]
"""