#!/usr/bin/env python3

import output
from datetime import datetime, timedelta

def BATTalert(listSatelitesDicts):
    
    alertBATT=0
    notifyAlertBATT=False
    intervalTime=0
    countOfSateliteAlertsDicts={}
    listOfSatellites=[]

    # If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
    for s in listSatelitesDicts:  

#        if s["component"] == "BATT": print(s["component"])
        
        startTimeBATT = datetime.strptime(s["timestamp"],
            "%Y%m%d %H:%M:%S.%f").timestamp() * 1000
        
        if alertBATT == 0:
            firstAlertTimestamp=s["timestamp"]
            
        #print(startTimeBATT)
        
        if s["component"]=="BATT":
            if intervalTime <= 0 or intervalTime > 300000:
                startTimeBATT=datetime.strptime(s["timestamp"],
                    "%Y%m%d %H:%M:%S.%f").timestamp() * 1000
                #print("if intervalTime <= 0 or intervalTime > 300000: ", startTimeBATT)
                
            if float(s["raw-value"]) < float(s["red-low-limit"]): 
                alertBATT=alertBATT+1
                         
                intervalTime=datetime.strptime(s["timestamp"],
                    "%Y%m%d %H:%M:%S.%f").timestamp() * 1000 - startTimeBATT
                
                #print("if s['raw-value'] < s['red-low-limit']: ", intervalTime)
                #print(s["satellite-id"])
                
        if intervalTime < 300000:
            notifyAlertBATT=True
            if alertBATT >= 3:
                countOfSateliteAlertsDicts["satellite_id"]=s["satellite-id"]                    
                countOfSateliteAlertsDicts["count"]=alertBATT   
                countOfSateliteAlertsDicts["timestamp"]=firstAlertTimestamp       
                intervalTime=0
                alertBATT=0
                
                # add to listOfSatelletes new countOfSateliteAlertsDicts
                dictionary_copy = countOfSateliteAlertsDicts.copy()
                listOfSatellites.append(dictionary_copy)
                
    #print(listOfSatellites)    

    notifyAlertBATT, json = output.printSateliteAlert(listOfSatellites,"BATT")

    return notifyAlertBATT, json
           
# If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
def TSTATalert(listSatelitesDicts):

    alertTSTAT=0
    notifyAlertTSTAT=False
    intervalTime=0
    countOfSateliteAlertsDicts={}
    listOfSatellites=[]

    for s in listSatelitesDicts:  
        
#        if s["component"] == "TSTAT": print(s["component"])
        
        startTimeTSTAT = datetime.strptime(s["timestamp"],
            "%Y%m%d %H:%M:%S.%f").timestamp() * 1000
        
        if alertTSTAT == 0:
            firstAlertTimestamp=s["timestamp"]

        #print(startTimeTSTAT)
        
        if s["component"]=="TSTAT":
            if intervalTime <= 0 or intervalTime > 300000:
                startTimeTSTAT=datetime.strptime(s["timestamp"],
                    "%Y%m%d %H:%M:%S.%f").timestamp() * 1000  
                #print("if intervalTime == 0 or intervalTime > 300000: ", startTimeTSTAT)
                
            if float(s["raw-value"]) > float(s["red-high-limit"]): 
                alertTSTAT=alertTSTAT+1
                
                intervalTime=datetime.strptime(s["timestamp"],
                    "%Y%m%d %H:%M:%S.%f").timestamp() * 1000 - startTimeTSTAT
                
                #print("if s['raw-value'] > s['red-high-limit']: ", intervalTime)
                #print(s["satellite-id"])
                #print(intervalTime)
                
        if intervalTime < 300000:
            notifyAlertTSTAT=True
            #print("intervalTime  < 300000:")
            if alertTSTAT >= 3:
                countOfSateliteAlertsDicts["satellite_id"]=s["satellite-id"]                    
                countOfSateliteAlertsDicts["count"]=alertTSTAT   
                countOfSateliteAlertsDicts["timestamp"]=firstAlertTimestamp       

                intervalTime=0
                alertTSTAT=0
                
                # add to listOfSatelletes new countOfSateliteAlertsDicts
                dictionary_copy = countOfSateliteAlertsDicts.copy()
                listOfSatellites.append(dictionary_copy)
                
    #print(listOfSatellites)    

    notifyAlertTSTAT,json = output.printSateliteAlert(listOfSatellites,"TSTAT")

    return notifyAlertTSTAT, json