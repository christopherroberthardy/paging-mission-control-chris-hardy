#!/usr/bin/env python3

import sys
import os
import input
import calculate_alerts
def main(testFile):
    
    fileName = testFile
    battResult=False
    jsonBATT={}
    tstatResult=False
    jsonTSTAT={}
    
    if os.path.isfile(fileName):
        
        listSatelitesDicts = input.read_file_into_json_object(fileName)
        
        battResult, jsonBATT = calculate_alerts.BATTalert(listSatelitesDicts)
        tstatResult, jsonTSTAT = calculate_alerts.TSTATalert(listSatelitesDicts)
        
        if battResult == True:
            print("Alert -  BATTERY voltage is LOW")

        if tstatResult == True:
            print("Alert -  Satellite Temperature is too HIGH") 
    else:
        print("File path {} does not exist. Exiting...".format(fileName))
    return jsonBATT, jsonTSTAT