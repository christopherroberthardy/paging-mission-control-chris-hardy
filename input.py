#!/usr/bin/env python3

listOfSatelites = []
sateliteDict = {}

# read input in line by line 
# store in an List of dictionary objects which split on '|' delinmeter one line at a time

# change list / dictionary object to json object by
# building json with the following data

# <timestamp> (unique value of dictionary) |<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>

def read_file_into_json_object(fileName):

   listOfSateliteDicts = []
    
   try:
      with open(fileName) as f:
         listOfSatelites = f.readlines()
   finally:
      f.close()

   #print(listOfSatelites)
   
   for x in listOfSatelites:
      strSplitOfSatelite=x.split("|")

      #print(strSplitOfSatelite)
      
      sateliteDict["timestamp"] = strSplitOfSatelite[0]       
      sateliteDict["satellite-id"] = strSplitOfSatelite[1]
      sateliteDict["red-high-limit"] = strSplitOfSatelite[2]
      sateliteDict["yellow-high-limit"] = strSplitOfSatelite[3]
      sateliteDict["yellow-low-limit"] = strSplitOfSatelite[4]
      sateliteDict["red-low-limit"] = strSplitOfSatelite[5]
      sateliteDict["raw-value"] = strSplitOfSatelite[6]
      sateliteDict["component"] = strSplitOfSatelite[7].rstrip()

      #print(sateliteDict)
      
      dictionary_copy = sateliteDict.copy()
      
      listOfSateliteDicts.append(dictionary_copy)

   #print(listOfSateliteDicts)

   return listOfSateliteDicts


# Example JSON String

"""
[
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   },
   {
      "timestamp":"20180101 23:05:07.421",
      "satellite-id":"1001",
      "red-high-limit":"17",
      "yellow-high-limit":"15",
      "yellow-low-limit":"9",
      "red-low-limit":"8",
      "raw-value":"7.9",
      "component":"BATT"
   }
]
"""

# return json_object when have read the whole input file


